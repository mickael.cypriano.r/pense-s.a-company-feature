
const firebaseConfig = {
  apiKey: "AIzaSyDT-OakcXoiOEM_cyEjhxDFb-suL29KxzA",
  authDomain: "flixzone-c6672.firebaseapp.com",
  databaseURL: "https://flixzone-c6672.firebaseio.com",
  projectId: "flixzone-c6672",
  storageBucket: "flixzone-c6672.appspot.com",
  messagingSenderId: "856333203241",
  appId: "1:856333203241:web:3f46c259afccc986a36938",
  measurementId: "G-94JQC3TT17"
}

firebase.initializeApp(firebaseConfig);

 // Envia dados para o banco de dados
 function enviarDados() {

    // Obter dados do formulário
    var nome = document.getElementById("nome").value;
    var email = document.getElementById("email").value;
    var menssagem = document.getElementById("menssagem").value

    var dados = {
      nome: nome,
      email: email,
      menssagem: menssagem
    }

    // Gera uma chave única para os dados
    var database = firebase.database();
    var novaChave = database.ref().child('usuarios').push().key;

    // Salva os dados no caminho 'usuarios/ID_ÚNICO'
    database.ref('usuarios/' + novaChave).set(dados)
    .then(function() {
        exibirToast("Bem-vindo, " + dados.nome + "! Sua mensagem foi enviada.");
      })
      .catch(function(error) {
        alert("Erro ao enviar dados: ", error);
      });
  }

  function exibirToast(mensagem) {
    alert(mensagem)
  }

  function cadastrarUsuario() {
    var email = document.getElementById('email').value;
    var senha = document.getElementById('senha').value;
    var userType = document.querySelector('input[name="userType"]:checked');
    var tipoUsuario = userType.id;


    firebase.auth().signInWithEmailAndPassword(email, senha)
    .then(function (userCredential) {
        var user = userCredential.user;

        // Agora, obtemos as informações adicionais do usuário do Firestore
        firebase.firestore().collection('usuarios').doc(user.uid).get()
            .then(function (doc) {
                if (doc.exists) {
                    var tipoUsuario = doc.data().tipoUsuario;

                    // Aqui você pode adicionar a lógica adicional com base no tipo de usuário
                    if (tipoUsuario == "0") {
                        window.location.href = "freelancer.html";
                    } else {
                        // Redirecionar para outra página, se necessário
                        // window.location.href = "outra_pagina.html";
                    }
                } else {
                    console.error('As informações do usuário não foram encontradas no Firestore.');
                }
            })
            .catch(function (error) {
                console.error('Erro ao obter informações do usuário do Firestore:', error);
            });
    })
    .catch(function (error) {
        console.error('Erro de autenticação:', error.message);
    });
}
