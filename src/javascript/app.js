var app = angular.module("app", []);


app.controller("MainController", function ($scope) {
  $scope.cards = [
    {
      title: "Mickael Cypriano da Rocha",
      content:
        "Olá, meu nome é Mickael e sou um apaixonado desenvolvedor front-end. Minha jornada no mundo do desenvolvimento web começou com a fascinação por criar interfaces envolventes e amigáveis para os usuários. Como profissional dedicado, busco constantemente aprimorar minhas habilidades e explorar as últimas tendências tecnológicas no universo do front-end. ",
      image:
        "https://lh3.googleusercontent.com/a/ACg8ocL1eutZqf7cvCQVoUDWpLzcE5IaszR4dvT4Xz-aJEqn68U=s288-c-no",
    },
  ];

  $scope.socialLinks = [
    {
      icon: "https://img.shields.io/badge/WhatsApp-25D366?style=for-the-badge&logo=whatsapp&logoColor=white",
      url: "https://api.whatsapp.com/send/?phone=%2B5522981369027&text=Ol%C3%A1+Pense+S.A+Company%2C+eu+tenho+uma+pergunta.&type=phone_number&app_absent=0",
    },
    {
      icon: "https://img.shields.io/badge/-twitter-%23333?style=for-the-badge&logo=twitterl&logoColor=white",
      url: "https://twitter.com/mickaelr442",
    },
    {
      icon: "https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedIn&logoColor=white",
      url: "https://www.linkedin.com/in/mickael-cypriano-da-rocha-203245281?utm_source=share&utm_campaign=share_via&utm_content=profile&utm_medium=android_app",
    },
    {
      icon: "https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white",
      url: "https://instagram.com/Mic_ofcial",
    },
  ];

  $scope.services = [
    {
      category: "Web Design",
      items: [
        "Responsive Web Design",
        "User Interface (UI) Design",
        "User Experience (UX) Design",
      ],
    },
    {
      category: "Web Development",
      items: [
        "Front-End Development",
        "Back-End Development",
        "Full-Stack Development",
      ],
    },
    {
      category: "E-commerce Solutions",
      items: [
        "Online Store Development",
        "Payment Gateway Integration",
        "Shopping Cart Solutions",
      ],
    },
  ];

  $scope.selectedTab = 0;

  $scope.toggleDarkMode = function () {
    document.body.classList.toggle("dark-mode");
  };

  $scope.showTabContent = function (index) {
    $scope.selectedTab = index;
  };

  // Seu código existente...
  
    // Adicione o array de planos de contratantes
    $scope.contractorPlans = [
      {
        title: 'FRONTEND',
        subtitle: 'Ideal pra remontagem de sites já montado',

        features: ['Recursos básicos', 'Suporte limitado']
      },
      {
        title: 'BACKEND',
        subtitle: 'Para melhor desempenho e fluxo de dados',
        features: ['Recursos avançados', 'Suporte prioritário']
      },
      {
        title: 'DESINGNER UX/UI',
        subtitle: 'Para redesenhar  a experiência do usuário',
        features: ['Recursos premium', 'Suporte 24/7']
      }
    ];
  
    // Função para ser chamada ao selecionar um plano
    $scope.selectPlan = function (plan) {
      // Lógica para lidar com a seleção do plano
      console.log('Plano selecionado:', plan.title);
    };
  
    // Seu código existente...
});

// Activate the Bulma navbar burger
document.addEventListener("DOMContentLoaded", function () {
  var burger = document.querySelector(".navbar-burger");
  var menu = document.querySelector(".navbar-menu");
  burger.addEventListener("click", function () {
    burger.classList.toggle("is-active");
    menu.classList.toggle("is-active");
  });
    
  });

  


