// function logar() {
//     window.open('src\page\login.html', '_self');
//   }


//   function cadastro() {
//     window.open('src\page\cadastro.html', '_self');
//   }
  

function logar() {
    // Usando barras normais
    // window.location.href = 'src/page/login.html';
  
    // Ou usando barras duplas
    window.location.href = 'src\\page\\login.html';
  }
  
  function cadastro() {
    // Usando barras normais
    // window.location.href = 'src/page/cadastro.html';
  
    // Ou usando barras duplas
    window.location.href = 'src\\page\\cadastro.html';
  }

  function verificarTipoUsuario() {
    // Obtém todos os inputs radio com o nome 'userType'
    var radioButtons = document.getElementsByName('userType');

    // Itera sobre os radio buttons para encontrar o selecionado
    for (var i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked) {
            // Obtém o valor do id do radio selecionado
            var userType = radioButtons[i].id;

 // Redireciona com base no userType
if (userType === 'frelancer') {
  window.location.href = '/src/page/freelancer.html';
} else if (userType === 'contratante') {
  window.location.href = '/src/page/contratante.html';
}

            // Interrompe o loop assim que encontrar o selecionado
            break;
        }
    }
}

function redirecionarCadastro() {
    // Lógica para redirecionar para a página de cadastro
    window.location.href = 'url_para_pagina_de_cadastro';
}